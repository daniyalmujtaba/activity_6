package com.example.activity_6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class Student {
    private String First_name;
    private String Last_name;
    private String age;
    private String enroll_no;
    private String class_c;

    public String getFirst_name() {
        return First_name;
    }

    public void setFirst_name(String first_name) {
        First_name = first_name;
    }

    public String getLast_name() {
        return Last_name;
    }

    public void setLast_name(String last_name) {
        Last_name = last_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEnroll_no() {
        return enroll_no;
    }

    public void setEnroll_no(String enroll_no) {
        this.enroll_no = enroll_no;
    }

    public String getClass_c() {
        return class_c;
    }

    public void setClass_c(String class_c) {
        this.class_c = class_c;
    }


}